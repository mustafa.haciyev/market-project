FROM openjdk:17
COPY build/libs/MarketProject-0.0.1-SNAPSHOT.jar /market-app1/
CMD ["java", "-jar","/market-app1/MarketProject-0.0.1-SNAPSHOT.jar"]
