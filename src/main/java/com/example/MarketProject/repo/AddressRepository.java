package com.example.MarketProject.repo;

import com.example.MarketProject.dto.AddressRequestDto;
import com.example.MarketProject.dto.BranchRequestDto;
import com.example.MarketProject.entity.Address;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address,Long>, JpaSpecificationExecutor<Address> {

    @Query(value = "select new com.example.MarketProject.dto.AddressRequestDto(a.addressName) from Address a")
    List<AddressRequestDto> getAddressWithQueryAll();

}
