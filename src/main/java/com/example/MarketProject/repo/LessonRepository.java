package com.example.MarketProject.repo;

import com.example.MarketProject.dto.LessonRequestDto;
import com.example.MarketProject.entity.Lesson;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LessonRepository extends JpaRepository<Lesson,Long> {

    Optional<Lesson> findByLessonName(String lessonName);

    @Query("select new com.example.MarketProject.dto.LessonRequestDto(l.lessonName, s.studentName) from Lesson l join l.students s")
    List<LessonRequestDto> getAllLessons();
}
