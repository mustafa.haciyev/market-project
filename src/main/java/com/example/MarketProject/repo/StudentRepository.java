package com.example.MarketProject.repo;

import com.example.MarketProject.dto.StudentRequestDto;
import com.example.MarketProject.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {

    Optional<Student> findByStudentName(String studentName);

}
