package com.example.MarketProject.repo;

import com.example.MarketProject.dto.BranchQueryDto;
import com.example.MarketProject.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BranchRepository extends JpaRepository<Branch,Long> , JpaSpecificationExecutor<Branch> {
    @Query(value = "select new com.example.MarketProject.dto.BranchQueryDto(b.id, b.branchName, a.addressName, m.marketName)" +
            " from Branch b join b.market m join b.address a")
    List<BranchQueryDto> getBranchByWithQuery();
}
