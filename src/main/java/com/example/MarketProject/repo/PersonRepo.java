package com.example.MarketProject.repo;

import com.example.MarketProject.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepo extends JpaRepository<Person,Long> {
}
