package com.example.MarketProject.repo;

import com.example.MarketProject.dto.AddressRequestDto;
import com.example.MarketProject.dto.MarketRequestDto;
import com.example.MarketProject.entity.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketRepository extends JpaRepository<Market,Long>, JpaSpecificationExecutor<Market> {

    @Query(value = "select new com.example.MarketProject.dto.MarketRequestDto(m.marketName) from Market m")
    List<MarketRequestDto> getMarketWithQueryAll();

}
