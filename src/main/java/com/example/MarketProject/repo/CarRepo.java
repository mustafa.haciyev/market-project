package com.example.MarketProject.repo;

import com.example.MarketProject.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CarRepo extends JpaRepository<Car,Long> {
    Optional<Car> findByBrand(String brand);
}
