package com.example.MarketProject.dto;

import com.example.MarketProject.entity.Car;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersonDto {
    private String name;
    private Set<String> carBrands;
}
