package com.example.MarketProject.dto;


import com.example.MarketProject.entity.Student;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Set;

@Data



public class LessonRequestDto {

    private String name;
    private Set<String> studentNames;

    public LessonRequestDto() {
    }

    public LessonRequestDto(String name, Set<String> studentNames) {
        this.name = name;
        this.studentNames = studentNames;
    }
}
