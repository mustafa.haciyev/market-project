package com.example.MarketProject.dto;

import com.example.MarketProject.entity.Lesson;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Set;

@Data


public class StudentRequestDto {

    private String name;
    private Set<String> lessonNames;

    public StudentRequestDto(String name, Set<String> lessonNames) {
        this.name = name;
        this.lessonNames = lessonNames;
    }
}
//@Service
//public class StudentServiceImpl implements StudentService {
//    @Autowired
//    private StudentRepository studentRepository;
//
//    @Autowired
//    private LessonRepository lessonRepository;
//
//    @Override
//    public Student createStudent(StudentDTO studentDTO) {
//        Student student = new Student();
//        student.setStudentName(studentDTO.getStudentName());
//        return studentRepository.save(student);
//    }
//
//    @Override
//    public List<Student> getAllStudents() {
//        return studentRepository.findAll();
//    }
//
//    @Override
//    public Student getStudentById(Long id) {
//        return studentRepository.findById(id)
//                .orElseThrow(() -> new ResourceNotFoundException("Student not found with id: " + id));
//    }
//
//    @Override
//    public void addLessonToStudent(Long studentId, Long lessonId) {
//        Student student = getStudentById(studentId);
//        Lesson lesson = lessonRepository.findById(lessonId)
//                .orElseThrow(() -> new ResourceNotFoundException("Lesson not found with id: " + lessonId));
//
//        student.getLessons().add(lesson);
//        studentRepository.save(student);
//    }
//}
