package com.example.MarketProject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BranchQueryDto {
    private Long branchId;
    private String branchName;
    private String marketName;
    private String addressName;

}
