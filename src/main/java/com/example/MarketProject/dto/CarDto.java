package com.example.MarketProject.dto;

import com.example.MarketProject.entity.Car;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CarDto {
    String brand;

    public Car toEntity() {
        Car car = new Car();
        car.setBrand(this.brand);
        return car;
    }
}
