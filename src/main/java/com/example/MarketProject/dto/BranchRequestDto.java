package com.example.MarketProject.dto;


import com.example.MarketProject.entity.Address;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchRequestDto {

    String branchName;
    String marketName;
    String addressName;
//    Address address;


}
