package com.example.MarketProject.service;

import com.example.MarketProject.dto.BranchQueryDto;
import com.example.MarketProject.dto.BranchRequestDto;
import com.example.MarketProject.entity.Branch;

import java.util.List;

public interface BranchService {
    long saveBranch(BranchRequestDto request);

    List<BranchQueryDto> getBranch();

    Branch getBranchById(Long id);

    List<Branch> findSpec(String name);
}
