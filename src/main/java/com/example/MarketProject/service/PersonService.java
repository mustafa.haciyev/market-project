package com.example.MarketProject.service;

import com.example.MarketProject.dto.PersonDto;
import com.example.MarketProject.entity.Person;
import org.springframework.data.crossstore.ChangeSetPersister;

public interface PersonService {
    void save(PersonDto personDto);

    void savePersonWithCars(PersonDto personDto) throws ChangeSetPersister.NotFoundException;
}
