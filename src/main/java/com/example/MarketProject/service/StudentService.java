package com.example.MarketProject.service;

import com.example.MarketProject.dto.LessonRequestDto;
import com.example.MarketProject.dto.StudentRequestDto;
import com.example.MarketProject.entity.Lesson;
import com.example.MarketProject.entity.Student;
import com.example.MarketProject.repo.LessonRepository;
import com.example.MarketProject.repo.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final LessonRepository lessonRepository;
    private final ModelMapper modelMapper;
    public void addLessonToStudent(String studentName, String lessonName) {
        Student student = studentRepository.findByStudentName(studentName)
                .orElseGet(() -> {
                    Student newStudent = new Student();
                    newStudent.setStudentName(studentName);
                    return studentRepository.save(newStudent);
                });

        Lesson lesson = lessonRepository.findByLessonName(lessonName)
                .orElseGet(() -> {
                    Lesson newLesson = new Lesson();
                    newLesson.setLessonName(lessonName);
                    return lessonRepository.save(newLesson);
                });

        student.getLessons().add(lesson);
        studentRepository.save(student);
    }

    public List<StudentRequestDto> getALlStudents() {
        List<Student> students = studentRepository.findAll();
         return students.stream()
                 .map(student -> modelMapper.map(student,StudentRequestDto.class))
                         .collect(Collectors.toList());

    }

    public StudentRequestDto findByIdStudent(Long id) {
        Student student = studentRepository.findById(id).orElseThrow(() -> new RuntimeException("not student"));
        return modelMapper.map(student,StudentRequestDto.class);
    }
}
