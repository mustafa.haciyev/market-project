package com.example.MarketProject.service;

import com.example.MarketProject.dto.LessonRequestDto;

import java.util.List;

public interface LessonService {
    List<LessonRequestDto> getAllStudent();

    LessonRequestDto getStudentById(Long id);

//    LessonRequestDto createStudent(LessonRequestDto lessonRequestDto);

    void addStudentToLesson(String lesonName, String studentName);
}
