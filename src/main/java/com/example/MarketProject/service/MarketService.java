package com.example.MarketProject.service;

import com.example.MarketProject.dto.AddressRequestDto;
import com.example.MarketProject.dto.MarketRequestDto;
import com.example.MarketProject.entity.Market;
import com.example.MarketProject.exception.MarketNotFoundException;

import java.util.List;

public interface MarketService {
    List<MarketRequestDto> getAllMarket();

    Market getMarketById(Long id) throws MarketNotFoundException;

    Long createMarket(MarketRequestDto marketRequestDto);

    List<Market> findSpec(String name);
}
