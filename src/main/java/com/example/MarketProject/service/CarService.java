package com.example.MarketProject.service;

import com.example.MarketProject.dto.CarDto;
import org.springframework.data.crossstore.ChangeSetPersister;

public interface CarService {

    void save(CarDto carDto);

    CarDto getCarById(Long carId) throws ChangeSetPersister.NotFoundException;

    void saveCar(CarDto carDTO);
}
