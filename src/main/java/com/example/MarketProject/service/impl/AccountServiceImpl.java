package com.example.MarketProject.service.impl;

import com.example.MarketProject.entity.Account;
import com.example.MarketProject.repo.AccountRepo;
import com.example.MarketProject.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {
    private final AccountRepo accountRepo;

    @Override
    public long saveAccount(Account account) {
        return accountRepo.save(account).getId();
    }

    @Override
    @Transactional
    public Account getAccount(long id) {
        return accountRepo.findById(id).get();
    }

    @Override
    @SneakyThrows
    @Transactional
    public long updateAccount(Account account) {
        long threadId = Thread.currentThread().getId();
        log.info("Start transaction : {}", threadId );
        Account accountFromDB = accountRepo.findById(account.getId()).get();
        log.info("Thread id : {}, account : {}", threadId, accountFromDB);
        accountFromDB.setBalance(accountFromDB.getBalance() + account.getBalance() );
        Account accountFromDB2 = accountRepo.findById(2L).get();
        accountFromDB2.setBalance(accountFromDB2.getBalance() - account.getBalance());
        Thread.sleep(10000);
        log.info("Thread id : {}, modifyAccount : {}", threadId, accountFromDB);
        Account savedAccount = accountRepo.save(accountFromDB);
        accountRepo.save(accountFromDB2);
        log.info("Thread id : {}, savedAccount : {}", threadId, savedAccount);
        return savedAccount.getId();
    }

    @Override
    @Transactional
    public long updateAccount2(Account account) {
        long threadId = Thread.currentThread().getId();
        log.info("Start transaction : {}", threadId );
        Account accountFromDB = accountRepo.findById(account.getId()).get();
        log.info("Thread id : {}, account : {}", threadId, accountFromDB);
        accountFromDB.setBalance(accountFromDB.getBalance() + account.getBalance() );
        Account accountFromDB2 = accountRepo.findById(2L).get();
        accountFromDB2.setBalance(accountFromDB2.getBalance() - account.getBalance());
        log.info("Thread id : {}, modifyAccount : {}", threadId, accountFromDB);
        Account savedAccount = accountRepo.save(accountFromDB);
        accountRepo.save(accountFromDB2);
        log.info("Thread id : {}, savedAccount : {}", threadId, savedAccount);
        return savedAccount.getId();
    }


}
