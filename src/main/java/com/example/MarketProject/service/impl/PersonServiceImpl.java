package com.example.MarketProject.service.impl;

import com.example.MarketProject.config.AppConfigiration;
import com.example.MarketProject.dto.PersonDto;
import com.example.MarketProject.entity.Car;
import com.example.MarketProject.entity.Person;
import com.example.MarketProject.repo.CarRepo;
import com.example.MarketProject.repo.PersonRepo;
import com.example.MarketProject.service.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RequiredArgsConstructor
@Service
@Slf4j
public class PersonServiceImpl implements PersonService {

    private final PersonRepo personRepo;
    private final CarRepo carRepo;
    private final CarServiceImpl carService;


    @Override
    public void save(PersonDto personDto) {



    }

    @Override
    public void savePersonWithCars(PersonDto personDTO) {
//        Person person = new Person();
//        person.setName(personDTO.getName());
//
//        Set<Car> cars = new HashSet<>();
//        for (String carBrand : personDTO.getCarBrands()) {
//            Car car = carRepo.findByBrand(carBrand)
//                    .orElseGet(() -> {
//                        Car newCar = new Car();
//                        newCar.setBrand(carBrand);
//                        return carRepo.save(newCar);
//                    });
//            cars.add(car);
//        }
//
//        person.setCars(cars);
//        personRepo.save(person);
//   Hem person hem car eyni anda save olunur

//        Person person = new Person();
//        person.setName(personDTO.getName());
//
//        Set<Car> cars = new HashSet<>();
//        for (String carBrand : personDTO.getCarBrands()) {
//            Optional<Car> optionalCar = carRepo.findByBrand(carBrand);
//
//            if (optionalCar.isPresent()) {
//                // Araba varsa, var olan arabayı kullan
//                cars.add(optionalCar.get());
//            }else {
//                log.info("Car not found");
//            }
//        }
//
//        person.setCars(cars);
//        personRepo.save(person);
//        sadece person save olunur eger databasede yoxdusa save olunmur

            Person person = new Person();
            person.setName(personDTO.getName());

            Set<Car> cars = new HashSet<>();
            for (String carBrand : personDTO.getCarBrands()) {

                Car car = carService.getOrCreateCar(carBrand);
                cars.add(car);
            }

            person.setCars(cars);
            personRepo.save(person);

//            hem personu hem cari save edir ancaq carrepo isledilmeyib
        }

    }



