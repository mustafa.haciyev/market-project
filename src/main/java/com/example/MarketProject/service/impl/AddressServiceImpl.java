package com.example.MarketProject.service.impl;

import com.example.MarketProject.config.AppConfigiration;
import com.example.MarketProject.dto.AddressRequestDto;
import com.example.MarketProject.entity.Address;
import com.example.MarketProject.exception.AddressNotFoundException;
import com.example.MarketProject.repo.AddressRepository;
import com.example.MarketProject.service.AddressService;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final AppConfigiration appConfigiration;

    @Override
    public List<AddressRequestDto> getAllAddress() {
        List<AddressRequestDto> addressRequestDto = addressRepository.getAddressWithQueryAll();
        return addressRequestDto;
    }

    @Override
    public Address getAddressById(Long id) throws AddressNotFoundException {
        return addressRepository.findById(id)
                .orElseThrow(() -> new AddressNotFoundException("Address not found"));
    }
    @Override
    public Long createAddress(AddressRequestDto addressRequestDto) {
        Address map = appConfigiration.getMapper().map(addressRequestDto, Address.class);
        return addressRepository.save(map).getId();
    }

    public List<Address> findSpec(String name) {
       return addressRepository.findAll(equalBaku(name));
    }

    public Specification<Address> equalBaku(String name){
//        Specification<Address> specification = new Specification<Address>() {
//            @Override
//            public Predicate toPredicate(Root<Address> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
//                return criteriaBuilder.equal(root.get("addressName"),name);
//            }
//        };

        Specification<Address> specification1 = ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("addressName"), name));
        return specification1;
    }


}
