package com.example.MarketProject.service.impl;

import com.example.MarketProject.dto.LessonRequestDto;
import com.example.MarketProject.entity.Lesson;
import com.example.MarketProject.entity.Student;
import com.example.MarketProject.repo.LessonRepository;
import com.example.MarketProject.repo.StudentRepository;
import com.example.MarketProject.service.LessonService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {

    private final LessonRepository lessonRepository;
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;


    @Override
    public List<LessonRequestDto> getAllStudent() {
        List<Lesson> courses = lessonRepository.findAll();
        return courses.stream()
                .map(course -> modelMapper.map(course, LessonRequestDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public LessonRequestDto getStudentById(Long id) {
        Lesson course = lessonRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Course not found with id " + id));
        return modelMapper.map(course, LessonRequestDto.class);
    }

    @Override
    public void addStudentToLesson(String lessonName, String studentName) {
        Lesson lesson = lessonRepository.findByLessonName(lessonName)
                .orElseGet(() -> {
                    Lesson lesson1 = new Lesson();
                    lesson1.setLessonName(lessonName);
                   return lessonRepository.save(lesson1);
                });

        Student student = studentRepository.findByStudentName(studentName)
                .orElseGet(()-> {
                    Student student1 = new Student();
                    student1.setStudentName(studentName);
                    return studentRepository.save(student1);
                });
        lesson.getStudents().add(student);
        lessonRepository.save(lesson);

    }


}
