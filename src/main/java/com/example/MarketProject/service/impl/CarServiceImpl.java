package com.example.MarketProject.service.impl;

import com.example.MarketProject.config.AppConfigiration;
import com.example.MarketProject.dto.CarDto;
import com.example.MarketProject.entity.Car;
import com.example.MarketProject.repo.CarRepo;
import com.example.MarketProject.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepo carRepository;
    private final AppConfigiration appConfigiration;

    public void saveCar(CarDto carDTO) {
        Car car = new Car();
        car.setBrand(carDTO.getBrand());
         carRepository.save(car);
    }

    public Car getOrCreateCar(String brand) {
        Optional<Car> optionalCar = carRepository.findByBrand(brand);

        return optionalCar.orElseGet(() -> {
            Car newCar = new Car();
            newCar.setBrand(brand);
            return carRepository.save(newCar);
        });
    }

    @Override
    public void save(CarDto carDto) {

    }

    @Override
    public CarDto getCarById(Long carId) throws ChangeSetPersister.NotFoundException {
        return null;
    }
}
