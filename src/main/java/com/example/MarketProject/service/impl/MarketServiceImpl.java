package com.example.MarketProject.service.impl;

import com.example.MarketProject.config.AppConfigiration;
import com.example.MarketProject.dto.AddressRequestDto;
import com.example.MarketProject.dto.MarketRequestDto;
import com.example.MarketProject.entity.Address;
import com.example.MarketProject.entity.Market;
import com.example.MarketProject.exception.MarketNotFoundException;
import com.example.MarketProject.repo.MarketRepository;
import com.example.MarketProject.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.error.Mark;

import java.security.PublicKey;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {

    private final MarketRepository marketRepository;
    private final AppConfigiration appConfigiration;

    @Override
    public List<MarketRequestDto> getAllMarket() {
        List<MarketRequestDto> marketRequestDto = marketRepository.getMarketWithQueryAll();
        return marketRequestDto;
    }

    @Override
    public Market getMarketById(Long id) throws MarketNotFoundException {
        return marketRepository.findById(id)
                .orElseThrow(() -> new MarketNotFoundException("Market not found"));
    }
    @Override
    public Long createMarket(MarketRequestDto marketRequestDto) {
        Market map = appConfigiration.getMapper().map(marketRequestDto, Market.class);
        return marketRepository.save(map).getId();
    }

    @Override
    public List<Market> findSpec(String name) {
        return marketRepository.findAll(equalsMarketName(name));
    }

    public Specification<Market> equalsMarketName(String name){

        Specification<Market> specification = ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("marketName"),name));

        return specification;
    }

}
