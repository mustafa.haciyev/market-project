package com.example.MarketProject.service.impl;

import com.example.MarketProject.dto.BranchQueryDto;
import com.example.MarketProject.dto.BranchRequestDto;
import com.example.MarketProject.entity.Address;
import com.example.MarketProject.entity.Branch;
import com.example.MarketProject.entity.Market;
import com.example.MarketProject.exception.BranchNotFoundException;
import com.example.MarketProject.repo.BranchRepository;
import com.example.MarketProject.service.AddressService;
import com.example.MarketProject.service.BranchService;
import com.example.MarketProject.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.security.PublicKey;
import java.util.List;


@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketService marketService;
    private final AddressService addressService;
    private final ModelMapper modelMapper;

    @Override
    public long saveBranch(BranchRequestDto request) {
//        Address address = addressService.getAddressById(request.getAddressId());
//        Market market = marketService.getMarketById(request.getMarketId());
//        Branch branch = Branch.builder()
//                .branchName(request.getBranchName())
//                .address(address)
//                .market(market)
//                .build();
//        return branchRepository.save(branch).getId();

        Branch map = modelMapper.map(request, Branch.class);
        return branchRepository.save(map).getId();

    }

    @Override
    public List<BranchQueryDto> getBranch() {
        List<BranchQueryDto> branchByWithQuery = branchRepository.getBranchByWithQuery();
        return branchByWithQuery;
    }

    @Override
    public Branch getBranchById(Long id) {
        return branchRepository.findById(id)
                .orElseThrow(() -> new BranchNotFoundException("Branch not found"));
    }

    @Override
    public List<Branch> findSpec(String name) {
        return branchRepository.findAll(equalsBranchName(name));
    }

    public Specification<Branch> equalsBranchName(String name){
        Specification<Branch> specification = ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("branchName"),name));
        return specification;
    }
}
