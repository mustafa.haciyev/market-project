package com.example.MarketProject.service;

import com.example.MarketProject.entity.Account;

public interface AccountService {
    long saveAccount(Account account);

    Account getAccount(long id);

    long updateAccount(Account account);

    long updateAccount2(Account account);
}
