package com.example.MarketProject.service;

import com.example.MarketProject.dto.AddressRequestDto;
import com.example.MarketProject.entity.Address;
import com.example.MarketProject.exception.AddressNotFoundException;

import java.util.List;

public interface AddressService {
    List<AddressRequestDto> getAllAddress();

    Long createAddress(AddressRequestDto addressRequestDto);

    Address getAddressById(Long id) throws AddressNotFoundException;

    List<Address> findSpec(String name);
}
