package com.example.MarketProject.controller;

import com.example.MarketProject.dto.LessonRequestDto;
import com.example.MarketProject.dto.StudentRequestDto;
import com.example.MarketProject.service.LessonService;
import com.example.MarketProject.service.StudentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/lesson")
public class LessonController {
    private final LessonService lessonService;
    @GetMapping
    public List<LessonRequestDto> getAllStudent(){
        return lessonService.getAllStudent();
    }

    @GetMapping("/{id}")
    public LessonRequestDto getStudentById(@PathVariable Long id){
        return lessonService.getStudentById(id);
    }

    @PostMapping("/addStudent")
    public ResponseEntity<String> addLessonToStudent(@RequestBody Map<String, String> request) {
        String studentName = request.get("studentName");
        String lessonName = request.get("lessonName");
        lessonService.addStudentToLesson(lessonName, studentName);
        return ResponseEntity.ok("Lesson added to student successfully.");
    }

}
