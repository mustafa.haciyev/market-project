package com.example.MarketProject.controller;


import com.example.MarketProject.dto.LessonRequestDto;
import com.example.MarketProject.dto.StudentRequestDto;
import com.example.MarketProject.entity.Student;
import com.example.MarketProject.service.StudentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;


    @GetMapping
    public List<StudentRequestDto> getAllStudents(){
    return studentService.getALlStudents();
    }

    @GetMapping("/{id}")
    public StudentRequestDto getLessonById(@PathVariable Long id){
        return studentService.findByIdStudent(id);
    }


@PostMapping("/addLesson")
public ResponseEntity<String> addLessonToStudent(@RequestBody Map<String, String> request) {
    String studentName = request.get("studentName");
    String lessonName = request.get("lessonName");
    studentService.addLessonToStudent(studentName, lessonName);
    return ResponseEntity.ok("Lesson added to student successfully.");
}

}
