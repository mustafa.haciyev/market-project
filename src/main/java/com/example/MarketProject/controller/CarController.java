package com.example.MarketProject.controller;

import com.example.MarketProject.dto.CarDto;
import com.example.MarketProject.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/car")
@RequiredArgsConstructor
public class CarController {
    private final CarService carService;

    @GetMapping("/{carId}")
    public ResponseEntity<?> getCarById(@PathVariable Long carId) {
        try {
            CarDto carDto = carService.getCarById(carId);
            return ResponseEntity.ok(carDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error retrieving car: " + e.getMessage());
        }
    }
    @PostMapping("/saveCar")
    public ResponseEntity<String> saveCar(@RequestBody CarDto carDTO) {
        carService.saveCar(carDTO);
        return ResponseEntity.ok("Car saved successfully.");
    }

}
