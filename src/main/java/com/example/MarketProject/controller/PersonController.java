package com.example.MarketProject.controller;

import com.example.MarketProject.dto.CarDto;
import com.example.MarketProject.dto.PersonDto;
import com.example.MarketProject.entity.Person;
import com.example.MarketProject.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;


//    @PostMapping("/save")
//    public ResponseEntity<String> savePerson(@RequestBody PersonDto personDto) {
//        personService.save(personDto);
//        return ResponseEntity.ok("saved success");
//    }


    @PostMapping("/savePerson")
    public ResponseEntity<String> savePersonWithCars(@RequestBody PersonDto personDTO) throws ChangeSetPersister.NotFoundException {
        personService.savePersonWithCars(personDTO);
        return ResponseEntity.ok("Person with cars saved successfully.");
    }

}
