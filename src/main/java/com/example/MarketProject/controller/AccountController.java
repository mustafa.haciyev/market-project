package com.example.MarketProject.controller;

import com.example.MarketProject.entity.Account;
import com.example.MarketProject.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;
    @PostMapping
    public long saveAccount(@RequestBody Account account){
        return accountService.saveAccount(account);
    }

    @GetMapping
    public Account getAccount(@RequestParam long id){
        return accountService.getAccount(id);
    }

    @PutMapping("/1")
    public long updateAccount(@RequestBody Account account){
        return accountService.updateAccount(account);
    }

    @PutMapping("/2")
    public long updateAccount2(@RequestBody Account account){
        return accountService.updateAccount2(account);
    }

}
