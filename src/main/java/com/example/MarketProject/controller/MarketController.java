package com.example.MarketProject.controller;

import com.example.MarketProject.dto.AddressRequestDto;
import com.example.MarketProject.dto.MarketRequestDto;
import com.example.MarketProject.entity.Address;
import com.example.MarketProject.entity.Market;
import com.example.MarketProject.exception.MarketNotFoundException;
import com.example.MarketProject.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @GetMapping
    public List<MarketRequestDto> getMarketAll(){
        return marketService.getAllMarket();
    }

    @GetMapping("/{id}")
    public Market getAddressById(@PathVariable Long id) throws MarketNotFoundException {
        return marketService.getMarketById(id);
    }

    @PostMapping
    public Long createAddress(@RequestBody MarketRequestDto marketRequestDto){
        return marketService.createMarket(marketRequestDto);
    }

    @GetMapping("/by/{name}")
    public List<Market> getMarket(@PathVariable String name){
        return marketService.findSpec(name);
    }


}
