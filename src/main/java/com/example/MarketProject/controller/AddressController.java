package com.example.MarketProject.controller;

import com.example.MarketProject.dto.AddressRequestDto;
import com.example.MarketProject.entity.Address;
import com.example.MarketProject.exception.AddressNotFoundException;
import com.example.MarketProject.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @GetMapping
    public List<AddressRequestDto> getAllAddress(){
        return addressService.getAllAddress();
    }

    @GetMapping("/by/{id}")
    public Address getAddressById(@PathVariable Long id) throws AddressNotFoundException {
        return addressService.getAddressById(id);

    }

    @PostMapping
    public Long createAddress(@RequestBody AddressRequestDto addressRequestDto){
        return addressService.createAddress(addressRequestDto);
    }

    @GetMapping("/{name}")
    public List<Address> getAddresses(@PathVariable String name){
       return addressService.findSpec(name);
    }

}
