package com.example.MarketProject.controller;

import com.example.MarketProject.dto.BranchQueryDto;
import com.example.MarketProject.dto.BranchRequestDto;
import com.example.MarketProject.entity.Branch;
import com.example.MarketProject.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {
    private final BranchService branchService;

    @PostMapping
    public long saveBranch(@RequestBody BranchRequestDto request) {
        return branchService.saveBranch(request);
    }

    @GetMapping
    public List<BranchQueryDto> getBranch() {
        return branchService.getBranch();
    }

    @GetMapping("/{id}")
    public Branch getBranchById(@PathVariable Long id){
        return branchService.getBranchById(id);
    }

    @GetMapping("/by/{name}")
    public List<Branch> getBranch(@PathVariable String name){
        return branchService.findSpec(name);
    }
}