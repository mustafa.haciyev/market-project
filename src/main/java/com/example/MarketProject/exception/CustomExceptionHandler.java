package com.example.MarketProject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class CustomExceptionHandler {
    @ExceptionHandler(BranchNotFoundException.class)
//    public Map<String,String> handleException(BranchNotFoundException exception){
//        Map<String, String> error = new HashMap<>();
//        error.put("exceptionMessage:", exception.getLocalizedMessage());
//        return new HashMap<>();
//    }

    public ResponseEntity<String> branchException(BranchNotFoundException br){
        return new ResponseEntity<>("Error message: " + br.getMessage(),HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(AddressNotFoundException.class)
    public ResponseEntity<String> addressHandleException(AddressNotFoundException ex){
        return new ResponseEntity<>("Bir xeta oldu: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MarketNotFoundException.class)
    public ResponseEntity<String> marketException(MarketNotFoundException mr){
        return new ResponseEntity<>("Error message: " + mr.getMessage(),HttpStatus.OK);

    }


}
