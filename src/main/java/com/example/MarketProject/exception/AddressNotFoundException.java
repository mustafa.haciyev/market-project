package com.example.MarketProject.exception;

import com.example.MarketProject.entity.Address;

public class AddressNotFoundException extends Exception{
    public AddressNotFoundException(String message){
        super(message);
    }
}
