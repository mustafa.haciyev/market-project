package com.example.MarketProject.exception;

import com.example.MarketProject.entity.Branch;

public class BranchNotFoundException extends RuntimeException{
    public BranchNotFoundException(String message){
        super(message);
    }
}
