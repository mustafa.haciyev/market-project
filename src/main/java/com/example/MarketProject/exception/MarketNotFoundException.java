package com.example.MarketProject.exception;

import com.example.MarketProject.entity.Market;

public class MarketNotFoundException extends Exception{
    public MarketNotFoundException(String message){
        super(message);
    }
}
