package com.example.MarketProject.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Market {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String marketName;

    @OneToMany(mappedBy = "market")
    @JsonIgnore
    List<Branch> branch;

//    Set<Branch> branches = new HashSet<>();

}
